package com.kkalfas.activityrecognitionexample.core

import com.kkalfas.activityrecognitionexample.core.di.DaggerAppComponent
import dagger.android.support.DaggerApplication

class App : DaggerApplication() {
    override fun applicationInjector() = DaggerAppComponent.builder().withApplication(this).build()
}