package com.kkalfas.activityrecognitionexample.core.event

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject

object EventBus {

    private val bus: PublishSubject<Event> = PublishSubject.create()

    fun onNext(event: Event) = bus.onNext(event)

    fun onReceive(): Observable<out Event> = bus.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}