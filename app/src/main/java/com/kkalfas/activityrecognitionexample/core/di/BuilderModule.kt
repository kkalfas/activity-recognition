package com.kkalfas.activityrecognitionexample.core.di

import com.kkalfas.activityrecognitionexample.services.LocationService
import com.kkalfas.activityrecognitionexample.recognition.RecognitionModule
import com.kkalfas.activityrecognitionexample.recognition.ui.MainActivity
import com.kkalfas.activityrecognitionexample.recognition.ui.MainFragment
import com.kkalfas.activityrecognitionexample.services.ActivityRecognitionService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BuilderModule {

    @ContributesAndroidInjector
    abstract fun bindMainActivity() : MainActivity

    @ContributesAndroidInjector(modules = [RecognitionModule::class])
    abstract fun bindMainFragment() : MainFragment

    @ContributesAndroidInjector(modules = [RecognitionModule::class])
    abstract fun bindLocationService() : LocationService

    @ContributesAndroidInjector(modules = [RecognitionModule::class])
    abstract fun bindActivityRecognitionService() : ActivityRecognitionService
}