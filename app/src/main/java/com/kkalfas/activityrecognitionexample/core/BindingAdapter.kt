package com.kkalfas.activityrecognitionexample.core

import android.databinding.BindingAdapter
import android.widget.TextView

object BindingAdapter {

    @BindingAdapter("scrollText")
    @JvmStatic
    fun scrollText(textView: TextView, unused: String) {
        textView.post { textView.scrollTo(0, textView.bottom); }
    }
}