package com.kkalfas.activityrecognitionexample.core

import java.text.SimpleDateFormat
import java.util.*

inline fun Date.ftime(): String {
    val format = SimpleDateFormat("HH:mm:ss")
    return format.format(this)
}