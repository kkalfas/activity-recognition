package com.kkalfas.activityrecognitionexample.core.event

import com.google.android.gms.location.ActivityTransitionEvent

sealed class Event {
    data class ReceivedActivity(val event : ActivityTransitionEvent) : Event()
    data class ReceivedLocation(val latitude: Double, val longitude: Double, val time : Long) : Event()
}