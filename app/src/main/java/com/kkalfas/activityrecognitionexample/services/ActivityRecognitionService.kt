package com.kkalfas.activityrecognitionexample.services

import android.app.job.JobParameters
import android.app.job.JobService
import android.widget.Toast
import com.kkalfas.activityrecognitionexample.recognition.ActivityRecognitionTracker
import com.kkalfas.activityrecognitionexample.recognition.model.TransitionType
import dagger.android.AndroidInjection
import javax.inject.Inject

class ActivityRecognitionService : JobService() {

    @Inject
    lateinit var tracker: ActivityRecognitionTracker

    override fun onCreate() {
        Toast.makeText(this, "ActivityRecognitionService oncreate works", Toast.LENGTH_SHORT).show()
        AndroidInjection.inject(this)
        super.onCreate()
    }

    override fun onStartJob(params: JobParameters?): Boolean {
        Toast.makeText(this, "ActivityRecognitionService onstartjob works", Toast.LENGTH_SHORT).show()
        tracker.startTracking(TransitionType.STILL, TransitionType.WALKING)
        return true
    }

    override fun onStopJob(params: JobParameters?): Boolean = true
}

