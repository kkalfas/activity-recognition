package com.kkalfas.activityrecognitionexample.services

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.util.Log
import android.widget.Toast
import com.kkalfas.activityrecognitionexample.R
import com.kkalfas.activityrecognitionexample.core.ftime
import com.kkalfas.activityrecognitionexample.recognition.RecognitionTracker
import com.kkalfas.activityrecognitionexample.recognition.model.TransitionType
import dagger.android.AndroidInjection
import java.util.*
import javax.inject.Inject

class LocationService : Service() {

    private companion object {
        const val location_interval = 500L
        const val location_distance = 0f
        const val service_id = 987654321
    }

    lateinit var locationManager: LocationManager

    @Inject
    lateinit var recognitionTracker: RecognitionTracker

    override fun onBind(intent: Intent?) = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return START_NOT_STICKY
    }

    @SuppressLint("MissingPermission")
    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
        startForeground()
        locationManager = applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            location_interval,
            location_distance,
            gpsCallback
        )
        locationManager.requestLocationUpdates(
            LocationManager.NETWORK_PROVIDER,
            location_interval,
            location_distance,
            networkCallbacks
        )

        recognitionTracker.startTracking(
            TransitionType.ON_FOOT,
            TransitionType.STILL,
            TransitionType.WALKING,
            TransitionType.IN_VEHICLE,
            TransitionType.RUNNING)
    }

    override fun onDestroy() {
        Toast.makeText(applicationContext, "SERVICE KILLED", Toast.LENGTH_SHORT).show()
        super.onDestroy()
    }

    private val gpsCallback = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            val logLine = "${location.latitude}, ${location.longitude}, ${Date(location.time).ftime()}\n"
            Log.d("LocationListener", logLine)
        }

        override fun onProviderEnabled(p0: String?) {}
        override fun onProviderDisabled(p0: String?) {}
        override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {}
    }

    private val networkCallbacks = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            val logLine = "${location.latitude}, ${location.longitude}, ${Date(location.time).ftime()}\n"
            Log.d("LocationListener", logLine)
        }

        override fun onProviderEnabled(p0: String?) {}
        override fun onProviderDisabled(p0: String?) {}
        override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {}
    }

    private fun startForeground() {
        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel("my_service", "My Background Service")
            } else {
                ""
            }

        val notificationBuilder = NotificationCompat.Builder(this, channelId)
        val notification = notificationBuilder.setOngoing(true)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setCategory(Notification.CATEGORY_SERVICE)
            .build()
        startForeground(service_id, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val chan = NotificationChannel(
            channelId,
            channelName, NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }
}