package com.kkalfas.activityrecognitionexample.recognition

import dagger.Binds
import dagger.Module

@Module
abstract class RecognitionModule {

    @Binds
    abstract fun bindActivityRecognitionTracker(activityRecognitionTracker: ActivityRecognitionTracker) : RecognitionTracker
}