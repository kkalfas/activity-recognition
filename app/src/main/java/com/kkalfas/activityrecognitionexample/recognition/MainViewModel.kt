package com.kkalfas.activityrecognitionexample.recognition

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableField
import android.util.Log
import com.google.android.gms.location.ActivityTransition
import com.google.android.gms.location.ActivityTransitionEvent
import com.google.android.gms.location.DetectedActivity
import com.kkalfas.activityrecognitionexample.core.event.Event
import com.kkalfas.activityrecognitionexample.core.event.EventBus
import com.kkalfas.activityrecognitionexample.core.ftime
import com.kkalfas.activityrecognitionexample.recognition.model.UiModel
import com.kkalfas.activityrecognitionexample.recognition.model.TransitionType
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class MainViewModel @Inject constructor(
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    val locationLogLine = ObservableField<String>(UiModel.NotInitialized.Unknown.name)
    val activityLogLine = ObservableField<String>(UiModel.NotInitialized.Unknown.name)

    fun onDetectedTransitionEvent() {
        compositeDisposable.addAll(
            EventBus.onReceive()
                .ofType(Event.ReceivedActivity::class.java)
                .subscribe(this::updateUi),
            EventBus.onReceive()
                .ofType(Event.ReceivedLocation::class.java)
                .subscribe(this::updateUi)
        )
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    private fun updateUi(event: Event) {
        when (event) {
            is Event.ReceivedLocation -> {
                val logLine = "lat: ${event.latitude} lng: ${event.latitude} : time ${Date(event.time).ftime()}\n"
                Log.d("MainViewModel", logLine)
                locationLogLine.set(locationLogLine.get().plus(logLine))
            }
            is Event.ReceivedActivity -> {
                val e = event.event
                val logLine = "activity : ${activity(e.activityType)} " +
                        "transition : ${transition(e.transitionType)} " +
                        "elapsed time ${Date(e.elapsedRealTimeNanos).ftime()}\n"
                Log.d("MainViewModel", logLine)
                activityLogLine.set(activityLogLine.get().plus(logLine))
            }
        }
    }

    private fun transition(transitionType: Int): String {
        return when (transitionType) {
            ActivityTransition.ACTIVITY_TRANSITION_ENTER -> UiModel.Transition.ENTER.name
            ActivityTransition.ACTIVITY_TRANSITION_EXIT -> UiModel.Transition.EXIT.name
            else -> UiModel.NotInitialized.Unknown.name
        }
    }

    private fun activity(activityType: Int): String {
        return when (activityType) {
            DetectedActivity.IN_VEHICLE -> UiModel.Activity.IN_VEHICLE.name
            DetectedActivity.STILL -> UiModel.Activity.STILL.name
            DetectedActivity.WALKING -> UiModel.Activity.WALKING.name
            DetectedActivity.RUNNING -> UiModel.Activity.RUNNING.name
            else -> UiModel.NotInitialized.Unknown.name
        }
    }
}
