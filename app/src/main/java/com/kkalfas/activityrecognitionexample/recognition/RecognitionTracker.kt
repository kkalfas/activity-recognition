package com.kkalfas.activityrecognitionexample.recognition

import com.kkalfas.activityrecognitionexample.recognition.model.TransitionType

interface RecognitionTracker {

    fun startTracking(vararg transitions: TransitionType)

    fun stopTracking()
}