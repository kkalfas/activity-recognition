package com.kkalfas.activityrecognitionexample.recognition.ui

import android.content.Context
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kkalfas.activityrecognitionexample.R.layout.fragment_main
import com.kkalfas.activityrecognitionexample.databinding.FragmentMainBinding
import com.kkalfas.activityrecognitionexample.recognition.MainViewModel
import dagger.android.support.AndroidSupportInjection
import dagger.android.support.DaggerFragment
import javax.inject.Inject
import android.text.method.ScrollingMovementMethod



class MainFragment : DaggerFragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    @Inject
    lateinit var viewModel: MainViewModel

    lateinit var binding : FragmentMainBinding
    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, instance: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, fragment_main, container, false)
        binding.viewModel = viewModel
        binding.fragmentMainLocationLog.movementMethod = ScrollingMovementMethod()
        binding.fragmentMainActivityLog.movementMethod = ScrollingMovementMethod()
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.onDetectedTransitionEvent()
    }
}
