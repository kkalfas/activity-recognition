package com.kkalfas.activityrecognitionexample.recognition

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.google.android.gms.location.ActivityTransitionResult
import com.google.android.gms.location.DetectedActivity
import com.kkalfas.activityrecognitionexample.services.LocationService

class ActivityRecognitionReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {
        if (ActivityTransitionResult.hasResult(intent)) {
            ActivityTransitionResult.extractResult(intent)?.let {
                Toast.makeText(context, "Broadcastreceiver, intent received", Toast.LENGTH_SHORT).show()
                it.transitionEvents.forEach { activity ->
                    if (activity.transitionType == DetectedActivity.WALKING)
                        Toast.makeText(context, "Broadcastreceiver, received intent has WALKING", Toast.LENGTH_SHORT).show()
                    context.startForegroundService(Intent(context, LocationService::class.java))
                }
            }
        }
    }
}
