package com.kkalfas.activityrecognitionexample.recognition

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.google.android.gms.location.ActivityRecognition
import com.google.android.gms.location.ActivityRecognitionClient
import com.google.android.gms.location.ActivityTransition
import com.google.android.gms.location.ActivityTransitionRequest
import com.kkalfas.activityrecognitionexample.core.event.Event
import com.kkalfas.activityrecognitionexample.core.event.EventBus
import com.kkalfas.activityrecognitionexample.core.scope.MainScope
import com.kkalfas.activityrecognitionexample.recognition.model.TransitionType
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

class ActivityRecognitionTracker @Inject constructor(
    private val applicationContext: Context
): RecognitionTracker, MainScope {

    private val activityRecognitionClient : ActivityRecognitionClient = ActivityRecognition.getClient(applicationContext)
    private val pendingIntent = receiverPendingIntent()

    override fun startTracking(vararg transitions: TransitionType) {
        val activityTransition = getActivityTransition(transitions)
        val requests = ActivityTransitionRequest(activityTransition)

        launch {
            suspendCoroutine { continuation ->
                val requestActivityTransition = activityRecognitionClient.requestActivityTransitionUpdates(requests, pendingIntent)
                requestActivityTransition.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(applicationContext, "requestActivityTransition isSuccessful", Toast.LENGTH_SHORT).show()
                        continuation.resume(Unit)
                    } else {
                        val exception = task.exception!!
                        Toast.makeText(applicationContext, exception.message, Toast.LENGTH_SHORT).show()
                        continuation.resumeWithException(exception)
                    }
                }
            }
        }
    }

    override fun stopTracking() {
        launch {
            suspendCoroutine { continuation ->
                val removeActivityTransition = activityRecognitionClient.removeActivityTransitionUpdates(receiverPendingIntent())
                removeActivityTransition.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(applicationContext, "removeActivityTransition isSuccessful", Toast.LENGTH_SHORT).show()
                        continuation.resume(Unit)
                    } else {
                        val exception = task.exception!!
                        Toast.makeText(applicationContext, exception.message, Toast.LENGTH_SHORT).show()
                        continuation.resumeWithException(exception)
                    }
                }
            }
        }
    }

    private fun getActivityTransition(transitions: Array<out TransitionType>): List<ActivityTransition> {
        return transitions.flatMap { transitionType ->
            val transitionEnter = ActivityTransition.Builder()
                .setActivityType(transitionType.type)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
                .build()
            val transitionExit = ActivityTransition.Builder()
                .setActivityType(transitionType.type)
                .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
                .build()
            listOf(transitionEnter, transitionExit)
        }
    }

    private fun receiverPendingIntent() : PendingIntent {
        val intent = Intent(applicationContext, ActivityRecognitionReceiver::class.java)
        return PendingIntent.getBroadcast(applicationContext, 0, intent, 0)
    }
}
