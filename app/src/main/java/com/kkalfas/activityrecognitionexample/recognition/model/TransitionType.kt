package com.kkalfas.activityrecognitionexample.recognition.model

import com.google.android.gms.location.DetectedActivity

enum class TransitionType(val type: Int) {
    IN_VEHICLE(DetectedActivity.IN_VEHICLE),
    ON_FOOT(DetectedActivity.ON_FOOT),
    RUNNING(DetectedActivity.RUNNING),
    WALKING(DetectedActivity.WALKING),
    ON_BICYCLE(DetectedActivity.ON_BICYCLE),
    STILL(DetectedActivity.STILL)
}