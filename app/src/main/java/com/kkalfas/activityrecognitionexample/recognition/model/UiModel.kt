package com.kkalfas.activityrecognitionexample.recognition.model

object UiModel {

    enum class Activity {
        IN_VEHICLE,
        ON_FOOT,
        RUNNING,
        WALKING,
        ON_BICYCLE,
        STILL,
    }

    enum class Transition {
        ENTER,
        EXIT,
    }

    enum class  NotInitialized {
        Unknown
    }

}